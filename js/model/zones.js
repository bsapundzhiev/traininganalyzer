var Zones = {
    Z1:123,
    Z2:153,
    Z3:169,
    Z4:184,
    Z5:190, //MHR
    getZone: function(bpm) {
        if(bpm <= Zones.Z1) {
            return 0;
        }
        else if(bpm > Zones.Z1 && bpm <= Zones.Z2) {
            return 1;
        }
        else if(bpm > Zones.Z2 && bpm <= Zones.Z3) {
            return 2;
        }
        else if(bpm > Zones.Z3 && bpm <= Zones.Z4) {
            return 3;
        } else {
            return 4;
        }
    }
};