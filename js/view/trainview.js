var TrainView = {

    init: function () {

        this.attachEvents();
        //this.viewModel.dataParsed = this.dataParsed.bind(this);

    },

    attachEvents: function () {

    },

    showZones: function () {


    },

    showSummary: function() {
        document.getElementById('title').innerText = this.viewModel._gpx.name;
        var min = this.viewModel._gpx.getMin();
        var pece = this.viewModel._training.paceCalc(min, this.viewModel._gpx.getDistance());

        document.getElementById("trainingSummary").innerHTML = "<ul>"+
            "<li>Time: "+ this.viewModel._gpx.getTime() +"</li>" +
            "<li>distance: "+ this.viewModel._gpx.getDistance() +"</li>" +
            "<li>Pece: "+ pece +"</li>" +
            "<li>avghr: "+ this.viewModel._gpx.getHeartRateAvg() +"</li>" +
            "<li>Elevation: "+ this.viewModel._gpx.amax + "</li>"+
            "<li>VO(max): "+ this.viewModel._training.vomax(min, pece) +"</li>" +
            "</ul>";
    }
}