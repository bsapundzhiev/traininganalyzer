/**
 * Main view
 */
var MainView = {

    map: null,
    osm: null,
    track: null,

    init: function () {
        this.attachEvents();
        this.initMap();
        //commands binding
        this.viewModel.dataParsed = this.dataParsed.bind(this);
        this.viewModel.loadGeoJSON = this.loadGeoJSON.bind(this);
    },

    initMap: function() {
        // set up the map
        this.map = new L.Map('map');
        // create the tile layer with correct attribution
        var osmUrl='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
        var osmAttrib='Map data � <a href="http://openstreetmap.org">OpenStreetMap</a> contributors';

        this.osm = new L.TileLayer(osmUrl, {minZoom: 1, maxZoom: 20, attribution: osmAttrib});
        this.map.setView(new L.LatLng(43.0786, 25.6272), 13);
        this.map.addLayer(this.osm);
    },

    loadGeoJSON: function(geojsonFeature) {
        this.track = new L.geoJson(geojsonFeature);
        this.map.addLayer(this.track);
        this.map.panTo(geojsonFeature.coordinates[0].reverse());
        //L.marker(geojsonFeature.coordinates[0].reverse()).addTo(this.map);
    },

    onFileChanged: function(event) {
        this.viewModel.onFileSelect(event.target.files);
    },

    attachEvents: function() {
        document.getElementById('files').addEventListener("change", this.onFileChanged.bind(this));
    },

    dataParsed: function() {
        this.train.showSummary();
        this.graph.showHrChart();
        this.graph.showElevationChart();
    }
};