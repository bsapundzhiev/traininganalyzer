
/**
 * Author: Borislav Sapundzhiev (c) 2016
 * License: Beerware, MIT
 */
var app = {
    mainView: null,
    graphView: null,
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('DOMContentLoaded', this.onDeviceReady, false);
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        app.receivedEvent('deviceready');
    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {
        console.log("trainig analyzer init");
        ViewModel.init();
        this.mainView = Object.create(MainView);
        this.mainView.viewModel = ViewModel;
        this.mainView.init();
        this.graphView = Object.create(GraphView);
        this.graphView.viewModel = ViewModel;
        this.graphView.init();
        this.trainView = Object.create(TrainView);
        this.trainView.viewModel = ViewModel;
        this.graphView.init();
        //inject subviews
        this.mainView.graph = this.graphView;
        this.mainView.train = this.trainView;
    }
};

app.initialize();
