//
//http://www.ncbi.nlm.nih.gov/pmc/articles/PMC4213373/
//

var TrainingEffect = {
    tzones: null,

//http://www.cs.uml.edu/~phoffman/xcinfo2.html
/*
where t is the race time in minutes, and v is race velocity in meters per minute.
Maximum Oxygen Update (Max VO2) values for selected groups
and individuals are as follows:
General Population, Female, Aged 20-29: 35-43 ml/kg/min
General Population, Male, Aged  20-29: 44-51
US College Track, Male:             57.4
College Students, Male:             44.6
Highest Recorded Female (Cross-Country Skier): 74
Highest Recorded Male (Cross-Country Skier):  94
*/
    paceCalc: function(min, kmeters) {

       return (min / kmeters);
    },

    vomax: function(minutes, pace) {

        var v = 1609.3/ Number(pace); /* calc meters/mile*/
        /* VO2 and percent_max based on "Jack Daniels, Conditioning for
        Distance Running - The Scientific Aspects", Wiley & Sons, 1978 */
        var vo2 = -4.60 + 0.182258 * v + 0.000104 * v * v;
        var percent_max = 0.8 + 0.1894393 * Math.exp(-0.012778 * minutes)
            + 0.2989558 * Math.exp(-0.1932605 * minutes);
        var vo2max = vo2 / percent_max;
        return vo2max;
    },

    //http://djconnel.blogspot.bg/2011/08/strava-suffer-score-decoded.html
    //http://tech.thejoestory.com/2014/07/suffer-score.html
    calcSS: function(t1, t2, bpm) {
        var dd = new Date(t2).getTime() - new Date(t1).getTime();
        var z = Zones.getZone(bpm);
        this.tzones[z] += Number(dd);
    },

    getSS: function() {

        var K1 = 20, K2 = 50, K3=110, K4 =230, K5 = 470;
        //var K1=30,K2=60,K3=120,K4=240,K5=480;
        //var K1=12, K2=24, K3=48, K4=96, K5=192;

        var t1 = ((this.tzones[0] / 1000) / 60) / 60;
        var t2 = ((this.tzones[1] / 1000) / 60) / 60;
        var t3 = ((this.tzones[2] / 1000) / 60) / 60;
        var t4 = ((this.tzones[3] / 1000) / 60) / 60;
        var t5 = ((this.tzones[4] / 1000) / 60) / 60;

        var suffer_score = (K1 * t1) + (K2 * t2) + (K3 * t3) + (K4 * t4) + (K5 * t5);
        var red_points = (K4 * t4) + (K5 * t5);

        return {
            sufferScore: suffer_score.toFixed(0),
            redPoints: red_points.toFixed(0)
        };
    },

    //http://fellrnr.com/wiki/TRIMP
    getTRIMP: function(Tmin, hrAvg) {

        var HRmin = 60;
        var HRmax = Zones.Z5;
        var HRr = ((hrAvg-HRmin) / (HRmax-HRmin));
        var TRIMPexp = Tmin * HRr * 0.64 * Math.exp(1.92 * HRr);

        return TRIMPexp;
    },

    //
    //http://www.amstat.org/publications/jse/v22n2/laumakis.pdf
    //
    calcTE: function(hrAvg, hrMax) {

        //var TE =  -27.9052 + 0.4378 * hrAvg - 0.0015 * hrAvg * hrAvg;
        var TE =  -57.0810 + 0.3527 * Number(hrMax) + 0.4495 * Number(hrAvg) - 0.0026 * Number(hrMax) * Number(hrAvg);
        console.log(TE);
        return TE;
    }
}
